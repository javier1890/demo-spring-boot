package com.example.demo;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.example.demo.model.Equipos;
import com.example.demo.model.Investigadores;
import com.example.demo.model.Reserva;
import com.example.demo.repository.EquiposRepository;
import com.example.demo.repository.InvestigadoresRepository;


@SpringBootApplication
public class DemoApplication implements CommandLineRunner{

	
	@Autowired
	@Qualifier("EquiposRepository")
	private EquiposRepository repoEquipos;
	
	@Autowired
	private InvestigadoresRepository repoInvestigadores;
	
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}
	
	@Override
	public void run(String... args) throws Exception {
		//buscarTodosLosEquipos();
		//buscarPorId(10);
		//existePorId(225);
		//contarEquipos();
		//buscarEntreNumeros(10,11);
		//buscarPorNombre_O_Id("rayos x",19);
		//buscarMenoresCon_id(10);
		//buscarMenoresCon_id_O_iguales_a();
		//esNula();
		//esNoNula();
		//buscarSi_comienza_con_algunNombre("X");
		//buscarNo_tenga_algunNombreDe("x");
		//buscarLosQueNoTengan_Palabra_O_Letra("z");
		//ordenarValoresPorFacultd();
		//buscarNombre_Mayusculas_o_Minusculas("RAYOS x");
		//buscarPorFechas();
		//buscarEquiposPorInvestigador();
		//buscarinvestigadoresEquiposPorFacultad(2,5);
		//investigadorMenorId(5);
		//buscarPorFechas();
		////buscarPorIdReservas();
	}
	
	
	/**
	 * buscar todos los equipos
	 */
	public void buscarTodosLosEquipos() {
		List<Equipos> equipos=repoEquipos.findAll();		
	    int i=1;
		for (Equipos e : equipos) {
			System.out.println(i+")- "+e.getNombre());
			i++;
		}	
	}
	
	
	/**
	 * buscar por id
	 */
      public void buscarPorId(int id) {
    	Optional<Equipos> opcional=repoEquipos.findById(id);
    	
    	if (opcional.isPresent()) {
    		System.out.println(opcional.get().getNombre());
		}else {
    	System.out.println("no existe");
		}
      }
      
      
      /**
       * existe o no
       */
      public void existePorId(int id) {
    	  if (repoEquipos.existsById(id)) {
			System.out.println("Existe");
		}else {
			System.out.println("no existe");
		}
      }

      
      /**
       * contar
       */
      public void contarEquipos() {
    	  System.out.println(repoEquipos.count());
        }
      
      
      
      
      /**
       * buscar entre numeros
       */
      public void buscarEntreNumeros(int a,int b) {
    	  List<Equipos> eq=repoEquipos.findByNumeroSerieBetween(a,b);
    	  
    	  if (eq.size()>0) {
   		  for (Equipos equipo : eq) {
  			  System.out.println(equipo.getNombre());
   		  }
			
		}else {
			System.out.println("no existen valores");
		}
      }
      
     public void  buscarPorNombre_O_Id(String a,Integer b) {
    	    List<Equipos> op=repoEquipos.findByNombreOrNumeroSerie(a,b);
    	    
    	    if(op.size()>0) {
    	    	for (Equipos equipos : op) {
					System.out.println(equipos.getNombre());
				}
    	    }else {
    	    	System.out.println("no existe");
			}   	    
     }
     
     
     public void buscarMenoresCon_id(int a) {
    	 List<Equipos> equipos=repoEquipos.findFirst8ByNumeroSerieLessThan(a);   	 
    	 if (equipos.size()>0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		}
    	 else {
    		 System.out.println("no existe");
		}
     }
     
     
     public void buscarMenoresCon_id_O_iguales_a() {
    	 List<Equipos> equipos=repoEquipos.findByNumeroSerieLessThanEqual(5);
    	 if (equipos.size()>0) {
 			for (Equipos equipo : equipos) {
 				System.out.println(equipo.getNombre());
 			}
 		}
     	 else {
     		 System.out.println("no existe");
 		}
     }
      
     
	public void esNula() {
		List<Equipos> equipos = repoEquipos.findByNombreIsNull();
		if (equipos.isEmpty()) {
			System.out.println("no hay campos de nombres vacios");
		} else {
			System.out.println("si hay campos de nombres vacios");
		}
	}
     
     
	public void esNoNula() {
		List<Equipos> equipos = repoEquipos.findByNombreIsNotNull();
		if (equipos.isEmpty()) {
			System.out.println("no hay campos de nombres vacios");
		} else {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		}
	}
      
     
    public void buscarSi_comienza_con_algunNombre(String nombre) {
    	List<Equipos> equipos=repoEquipos.findByNombreLike("%"+nombre);    	
    	if (equipos.size()>0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		}else {
			System.out.println("No existen ninguna parabra que comience con "+nombre);
		}
    }
      
    public void buscarNo_tenga_algunNombreDe(String nombre) {
    	List<Equipos> equipos=repoEquipos.findByNombreNotLike("%"+nombre);    	
    	if (equipos.size()>0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		}else {
			System.out.println("No existen ninguna parabra que comience con "+nombre);
		}
    }
    
    public void buscarLosQueNoTengan_Palabra_O_Letra(String nombre){
    	List<Equipos> equipos=repoEquipos.findByNombreContaining(nombre);    	
    	if (equipos.size()>0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		}else {
			System.out.println("No existen ninguna parabra que comience con "+nombre);
		}
    }
    
	public void ordenarValoresPorFacultd() {
		List<Integer> valores = new ArrayList<Integer>();
		valores.add(1);
		valores.add(20);

		List<Equipos> equipos = repoEquipos.findByNumeroSerieIn(valores);
		if (equipos.size() > 0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNombre());
			}
		} else {
			System.out.println("No existen ninguna valores ");
		}
	}
      
	
	public void buscarNombre_Mayusculas_o_Minusculas(String nombre) {
		Optional<Equipos> equipos=repoEquipos.findByNombreIgnoreCase(nombre);
		
		if (equipos.isPresent()) {
			System.out.println(equipos.get().getNumero_serie()+")- "+equipos.get().getNombre());
		}else {
			System.out.println("no existe");
		}		
	}
	
	public void buscarPorFechas() {
		List<Equipos> equipos=repoEquipos.buscarEquipoPorFechas();
		for (Equipos equipo : equipos) {
			System.out.println(equipo.getNombre());
		}
	   
		
	}
	
	
	public void buscarEquiposPorInvestigador() {
		List<Equipos> equipos=repoEquipos.buscarEquiposXNombre_de_investigadores("javier");
		
		System.out.println("");
		
		if (equipos.size()>0) {
			for (Equipos equipo : equipos) {
				System.out.println(equipo.getNumero_serie()+") "+equipo.getNombre());
			}
		}else {
			System.out.println("no ha reservado equipos");
		}
	}
	
      
	
	public void buscarinvestigadoresEquiposPorFacultad(int a1,int a2) {
		
		List<Investigadores> investigadores=repoInvestigadores.buscarInvestigadores_queReservoEquipo_facultad(a1,a2);
		
		if (investigadores.size()>0) {
			for (Investigadores investigadores2 : investigadores) {
				System.out.println(investigadores2.getNombre());
			}
		}else {
			System.out.println("No existen investigadores");
		}
	}
	
	public void investigadorMenorId(int num) {
           List<Investigadores> investigadores=repoInvestigadores.investigadoresIdMayor(num);
		
		if (investigadores.size()>0) {
			for (Investigadores investigadores2 : investigadores) {
				System.out.println(investigadores2.getNombre());
			}
		}else {
			System.out.println("No existen investigadores");
		}
	}
	
//	public void buscarPorIdReservas() {
//		List<Reserva> reservaOptional=repoEquipos.buscarReserva();
//		
//	
//				System.out.println(reservaOptional.toArray());
//				
//		
//	}
	
	
      } 
      
